All states:
NSW (New South Wales) - Population: 7704300
VIC (Victoria) - Population: 6039100
SA (South Australia) - Population: 1706500
QL (Queensland) - Population: 4827000
TAS (Tasmania) - Population: 518500
WA (Western Australia) - Population: 2613700
ACT (Australian Capital Territory) - Population: 397397
NT (Northern Territory) - Population: 244000

NSW is the abbreviation for: New South Wales
Victoria exists: true
Least populated state: Northern Territory
Average population: 3006312.12
Number of states with over 10mn people: 0
Number of states with less than 1mn people: 3
Number of states with initials 'A': 1